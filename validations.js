function validateForm(form) {
  var firstName = form.firstName;
  if (validateName(firstName)) {
    var lastName = form.lastName;
    if (validateName(lastName)) {
      var email = form.email;
      if (email.value.includes("@"))
        alert("The info was send to server...")
      else
        alert("You have to enter a correct email address")
    }
  }
    
  
}

function validateName(element) {
  let msg = "You have to enter a " + element.name;
  var hasNumber = /\d/;

  if (element.value !== "") {
    if (!hasNumber.test(element.value))
      return true;
    else 
      msg = "You have to enter a " + element.name + " without numbers";
  }
  
  alert(msg);
  element.focus();
  element.select();
  element.value = "";
  return false;
}